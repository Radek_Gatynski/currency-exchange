let convertButton = document.getElementById('convert');
let cash = document.getElementById('cash');
let option = document.getElementById('selectedC');
let res = document.getElementById('resultsDiv');

let dataFromServer = '';
let avibleCURR = ['PLN', 'USD', 'EUR', 'CZK', 'RUB'];
let choseOption = '';


convertButton.addEventListener('click', function(){
    if(cash.value != 0 || cash.value != ''){
        if(res.childNodes.length){
            while(res.firstChild){
                res.removeChild(res.firstChild);
            }
         }
        choseOption = option.value;
        getDataFromServer();
    }
    else{
        alert("no cash not conver");
   }
});

function getDataFromServer(){
    var xhr = new XMLHttpRequest(),
    method = "GET",
    url = "https://api.ratesapi.io/api/latest?base="+choseOption+"&symbols="+avibleCURR;
    xhr.open(method, url, true);
    xhr.onreadystatechange = function () {
    if(xhr.readyState === 4 && xhr.status === 200) {        
        dataFromServer = xhr.responseText;
        makeConvert(dataFromServer);
    }
    };
    xhr.send()
}

function makeConvert(dataCash){
 dataCash = JSON.parse(dataCash);
 dataCash = dataCash.rates;
 let valueCurrency = Object.values(dataCash);
 let resultsCash = 0;
 let index = 0;
 for(let i in dataCash){
    let el = document.createElement("div");
    el.classList.add("single-element");
    let spanInf = document.createElement("span");
    spanInf.innerText = i;
    let hInfo = document.createElement("h3");
    resultsCash = Number(cash.value * valueCurrency[index]).toFixed(2);
    hInfo.innerText = resultsCash;
    el.appendChild(spanInf);
    el.appendChild(hInfo);
    res.appendChild(el);
    index++;
}

}