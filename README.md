## Currency convert
An application for converting your money into other currencies.

## Used technology
HTML, JS, SCSS

## How to run
Open index.html into your browser, after this you must enter your money into input field, select the money currency and click 'Convert'.
To convert money you need have internet connection, because main.js file used internet to get the current currency value.
You can edit the main.js file adding in variable 'avibleCURR' currency what you want get. 